#!/usr/bin/python3
# -*- coding: utf-8 -*-

yesno: str = input("Do you like chocolate")

if yesno == "y":
    choctype: str = input("Do you like dark chocolate?")
    if choctype == "y":
        print("Here's a cookie")
    else:
        print("Too bad")
else:
    print("You're a smarty")
